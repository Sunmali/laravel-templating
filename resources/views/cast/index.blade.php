@extends('layouts.master')

@section('mainjudul')
    <h1>Cast</h1>
@endsection

@section('subjudul')
    <h3 class="card-title">List Pemain Film</h3>
@endsection

@section('content')
<a href="/cast/create" type="button" class="btn btn-success mb-3">Tambah Data</a>
<table class="table table-hover">
    <thead class="bg-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody class="table-active">
        @forelse ($cast as $key => $data)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$data->nama}}</td>
            <td>{{$data->umur}}</td>
            <td>{{$data->bio}}</td>
            <td>
                <form action="/cast/{{$data->id}}" method="POST">
                    <a href="/cast/{{$data->id}}" type="button" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$data->id}}/edit" type="button" class="btn btn-warning btn-sm mx-1">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button name="submit" type="submit" class="btn btn-danger btn-sm">Delete</button>
                </form>
            </td>
        </tr>
        @empty
        <tr>
            <td>Null</td>
        </tr>  
        @endforelse
    </tbody>
</table>
@endsection
