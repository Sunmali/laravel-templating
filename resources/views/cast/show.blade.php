@extends('layouts.master')

@section('mainjudul')
    <h1>Halaman Detail Pemain Film</h1>
@endsection

@section('subjudul')
    <h3 class="card-title">Detail Pemain {{$cast->nama}}</h3>
@endsection

@section('content')
<div class="row">
    <h5 class="col-2 font-weight-bold">Nama</h5>
    <h5 class="col-10">: {{$cast->nama}}</h5>
    <h5 class="col-2 font-weight-bold">Umur</h5>
    <h5 class="col-10">: {{$cast->umur}}</h5>
    <h5 class="col-2 font-weight-bold">Bio</h5>
    <h5 class="col-10">: {{$cast->bio}}</h5>
</div>
<a href="/cast" type="button" class="btn btn-secondary mt-3">Kembali</a>
@endsection
