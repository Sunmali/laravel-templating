@extends('layouts.master')

@section('mainjudul')
    <h1>Halaman Form Cast</h1>
@endsection

@section('subjudul')
    <h3 class="card-title">Form Cast</h3>
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group row">
      <label for="nama" class="col-2 col-form-label">Nama</label> 
      <div class="col-10">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <i class="fa fa-address-book"></i>
            </div>
          </div> 
          <input id="nama" name="nama" type="text" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-warning small">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <div class="form-group row">
      <label for="umur" class="col-2 col-form-label">Umur</label> 
      <div class="col-10">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <i class="fa fa-birthday-cake"></i>
            </div>
          </div> 
          <input id="umur" name="umur" type="text" class="form-control"> 
          <div class="input-group-append">
            <div class="input-group-text">Tahun</div>
          </div>
        </div>
        @error('umur')
            <div class="alert alert-warning small">{{ $message }}</div>
        @enderror
      </div>
    </div>
    <div class="form-group row">
      <label for="textarea" class="col-2 col-form-label">Bio</label> 
      <div class="col-10">
        <textarea id="bio" name="bio" cols="40" rows="4" class="form-control"></textarea>
        @error('bio')
            <div class="alert alert-warning small">{{ $message }}</div>
        @enderror
      </div>
    </div> 
    <div class="form-group row">
      <div class="offset-2 col-9">
        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
</form>
@endsection


