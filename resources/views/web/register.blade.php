<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <p>First name:</p>
        <input type="text" id="fname" name="fname">
        <p>Last name:</p>
        <input type="text" id="lname" name="lname"><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label><br>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="bahasa" name="bahasa" value="Bahasa Indonesia">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="other" value="Other">
        <label for="other">Other</label><br>

        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="35" rows="10"></textarea><br>
        <button type="submit">Sign In</button>   
    </form>

</body>
</html>